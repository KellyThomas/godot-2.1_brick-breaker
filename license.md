FILE                                    | LICENSE   | CREDIT / COPYRIGHT    | CONTACT
----                                    | -------   | ------------------    | -------
license.txt                             | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/engine.cfg                | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/scenes/ball.tscn          | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/scenes/brick.tscn         | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/scenes/main.tscn          | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/scenes/paddle.tscn        | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/scripts/paddle.gd         | MPL-2.0   | � 2018 Kelly Thomas   | https://bitbucket.org/KellyThomas/
godot-project/sprites/ballGrey_10.png   | CC0-1.0   | Kenney                | https://kenney.nl/
godot-project/sprites/brickBlue02.png   | CC0-1.0   | Kenney                | https://kenney.nl/
godot-project/sprites/paddle_01.png     | CC0-1.0   | Kenney                | https://kenney.nl/
licenses/MPL-2.0.txt                    | MPL-2.0   | Mozilla Foundation    | https://www.mozilla.org/
licenses/CC0-1.0.txt                    | CC0-1.0   | Creative Commons      | https://creativecommons.org/
