# © 2018 Kelly Thomas

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

extends KinematicBody2D

const BALL_SCENE = preload("res://scenes/ball.tscn")

var mouse_x
var home_y

func _ready():
	set_process_input(true)
	set_fixed_process(true)
	home_y = get_pos().y
	mouse_x = get_pos().x

func _fixed_process(delta):
	set_pos(Vector2(mouse_x, home_y))

func _input(event):
	if event.type == InputEvent.MOUSE_MOTION:
		mouse_x = get_viewport().get_mouse_pos().x
	elif event.type == InputEvent.MOUSE_BUTTON:
		if event.is_pressed():
			var ball = BALL_SCENE.instance()
			ball.set_pos(get_pos() + Vector2(0,-32))
			get_tree().get_root().add_child(ball)
